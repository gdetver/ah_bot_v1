import datetime
import os
import sys
import paramiko

import telebot
from telebot import types
from ah_bot import bot

import config
import ah_sql
from ah_func import log_action, log_error, log_access,clear_vars,last_inline_set

#bot = telebot.TeleBot(config.TOKEN)

def abandon_agent_get_num(chat_id):
    bot.send_message(chat_id,'Введите 10 значный номер:')
    return

def abandon_agent_find_sql(chat_id):
    action_stage = config.current_actions.get(chat_id)
    now = datetime.datetime.now() 
    if now.hour < 4:
        start_int = datetime.datetime(now.year,now.month,now.day-1,4,00,00)
    else:
        start_int = datetime.datetime(now.year,now.month,now.day,4,00,00)
 
    bot.send_message(chat_id,'ищю пропущенные для номера: '+ str(action_stage[1]))
    result = ah_sql.select_abandon_agent(chat_id,action_stage[1],start_int) 

    log_action('abandon_agent_find_sql',chat_id, 'найдено пропущенных для номера: {!s}, количество  {!s}.'\
                    .format(action_stage[1],len(result)))

    if len(result) > 1:
        markup = types.InlineKeyboardMarkup()
        for item in result:
            print(item)
        for item in result:
            row = []
            row.append(types.InlineKeyboardButton(str(item[0]), callback_data="abandon-agent-"+str(item[1])))
            markup.row(*row)
        inl = bot.send_message(chat_id, 'Выберете интересующий звонок:', reply_markup=markup)
        last_inline_set(chat_id,inl.message_id)
        return
    elif len(result) == 1:
        abandon_agent_find_ssh(chat_id,result[0][1])
        return
    else:
        bot.send_message(chat_id,'для номера ' + str(action_stage[1])+ ' пропущенных не найдено')
        return

def abandon_agent_find_ssh(chat_id,unique):
    abandon = abandon_agent_ssh(unique)
    log_action('abandon_agent_find_ssh',chat_id, 'найдено в логе для uniqueid: {!s}, лог:  {!s}.'\
                    .format(unique,abandon))
    if abandon == "":
        bot.send_message(chat_id,'ошибка подключения к логу')
    else:
        bot.send_message(chat_id,abandon)
    return

def abandon_agent_ssh(unique):
    try:
        hostname = '10.10.10.10'
        myuser   = 'log'
        mySSHK   = './ssh/ssh.private'
        command = "cat /var/log/asterisk/full | grep -e '"+str(unique)+"' | sed 's|.*\]\[||' | sed -r 's/].+//' | uniq | xargs -i grep {} /var/log/asterisk/full | grep -e 'app_queue.c\|res_musiconhold' |  awk -F ']' '{print $1 $4}'"
        sshcon   = paramiko.SSHClient()  # will create the object
        sshcon.set_missing_host_key_policy(paramiko.AutoAddPolicy())# no known_hosts error
        sshcon.connect(config.SSH_AST_HOST, username=config.SSH_AST_USER, key_filename=config.SSH_AST_KEY)
        stdin, stdout, stderr = sshcon.exec_command(command)
        data = stdout.read() + stderr.read()
        return data
    except Exception as eX:
        log_error('ssh_client',eX)
        return ""




