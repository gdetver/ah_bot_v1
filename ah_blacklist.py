import telebot
from telebot import types
from ah_bot import bot
import json
from ah_ldap import ldap_main as ldap_search
from ah_sql import insert_blacklist_mdb, select_blacklist_mdb, update_blacklist_mdb, select_src_mdb
from ah_func import log_action, log_error, log_access, clear_vars, last_inline_set
import config
import datetime

def ban_number_decript(call):
    chat_id = call.from_user.id
    action_stage = config.current_actions.get(chat_id)
    number = call.text
    
    if len(number) > 10 or len(number) < 5:
        log_action('ban_number_decript',chat_id, 'неверная длинна номера =: {!s}'.format(config.current_actions[chat_id][1]))
        bot.send_message(chat_id, 'Неверная длина. Допускается ввод от 5 до 10 знаков.')
        ban_send_log('#pbx #pbxban','Неудачная опытка найти номер: '+str(config.current_actions[chat_id][1])[-5:],call)
        return
    else:
        config.current_actions[chat_id] = (action_stage[0],call.text)
        start_int = (datetime.datetime.now() - datetime.timedelta(hours=config.ban_decrypt_hour))
        stop_int = datetime.datetime.now()
        results = select_src_mdb(chat_id, number, str(start_int), str(stop_int))
        try:
            if len(results) > 1:
                bot.send_message(chat_id, 'Найдено более 1 номера, блокировка невозможна.\
                                 Обратитесь в техническую поддержку')
                log_action('ban_number_decript', chat_id, 'ошибка найдено номеров: {!s}'.format(len(results)))
                ban_send_log('#pbx #pbxban','Неудачная опытка найти номер: '+str(config.current_actions[chat_id][1])[-5:],call)
                return

            elif len(results) == 1:
                for items in results:
                    src = items[0]
                log_action('ban_number_decript', chat_id, 'номер {!s} найден'.format(src))
                config.current_actions[chat_id] = (action_stage[0], str(src))
                bot.send_message(chat_id, 'Введите причину блокировки номера с примерами.')
                return
            else:
                log_action('ban_number_decript', chat_id, 'попытка найти номер =: {!s}, поиск не дал результатов'.format(number))
                ban_send_log('#pbx #pbxban','Неудачная опытка найти номер: '+str(config.current_actions[chat_id][1])[-5:],call)
                bot.send_message(chat_id, 'Номер не найден.')

        except Exception as Ex:
            log_action('ban_number_decript', chat_id, 'попытка найти номер =: {!s}, ошибка =: {!s}'.format(number, Ex))
            bot.send_message(chat_id, 'Номер не найден.')
            ban_send_log('#pbx #pbxban','Неудачная опытка найти номер: '+str(config.current_actions[chat_id][1])[-5:],call)

# функция блокировки номера
def ban_number(chat_id):
    if len(config.current_actions[chat_id][1]) == 10:
        date_stop = config.current_shown_datetime.get(str(chat_id)+'-date-stop')
        time_stop = config.current_shown_datetime.get(str(chat_id)+'-time-stop')
        stop_int = datetime.datetime(date_stop[0],date_stop[1],date_stop[2],time_stop[0],time_stop[1],00)
        number = config.current_actions[chat_id][1]

        is_blocked = select_blacklist_mdb(chat_id,number)
        if is_blocked:
            bot.send_message(chat_id, 'Номер xxxxx' + number[5:] + ' уже заблокирован до: '+str(is_blocked[0][0]))
            kb = types.InlineKeyboardMarkup()
            kb.add(*[types.InlineKeyboardButton(text=text, callback_data="ban-accept-update-" + text) for text in ['да', 'нет']]) 
            inl = bot.send_message(chat_id, 'Заблокировать до: '+str(stop_int), reply_markup=kb)
            last_inline_set(chat_id,inl.message_id)
        else:
            bot.send_message(chat_id, 'Номер xxxxx' + number[5:] + ' будет заблокирован')
            kb = types.InlineKeyboardMarkup()
            kb.add(*[types.InlineKeyboardButton(text=text, callback_data="ban-accept-insert-" + text) for text in ['да', 'нет']])
            inl = bot.send_message(chat_id, 'Заблокировать до: '+str(stop_int), reply_markup=kb)
            last_inline_set(chat_id,inl.message_id)
    else:
        log_action('ban_number',chat_id, 'неверная длина номера =: {!s}'.format(number))
        bot.send_message(chat_id, 'Неверная длина.')

def ban_number_justification(chat_id, justification):
    number = config.current_actions[chat_id][1]
    if ldap_search(chat_id,config.ldap_admins):
        if len(justification) < 3:
            bot.send_message(chat_id, 'Слишком коротко. Пожалуйста введите больше данных.')
            log_action('ban_number_justification',chat_id,\
                       'обоснование для блокировки номера {!s} =: {!s}'.format(number, 'слишком короткое обоснование.'))
            return
    else:
        if len(justification) < 20:
            bot.send_message(chat_id, 'Слишком коротко. Пожалуйста введите больше данных.')
            log_action('ban_number_justification',chat_id,\
                       'обоснование для блокировки номера {!s} =: {!s}'.format(number, 'слишком короткое обоснование.'))
            return

    log_action('ban_number_justification',chat_id,\
               'обоснование для блокировки номера {!s} =: [{!s}]'.format(number, justification))
    config.current_justification[chat_id] = (str(justification))
    ban_number(chat_id)
    return

def ban_number_accept(call):
    justification = config.current_justification[call.from_user.id]
    number = config.current_actions[call.from_user.id][1]
    bot.answer_callback_query(callback_query_id=call.id, show_alert=False, text="")
    if call.data[18:] == 'да':
        print(call.data[11:17])
        date_stop = config.current_shown_datetime.get(str(call.from_user.id)+'-date-stop')
        time_stop = config.current_shown_datetime.get(str(call.from_user.id)+'-time-stop')
        stop_int = datetime.datetime(date_stop[0],date_stop[1],date_stop[2],time_stop[0],time_stop[1],00)
        if call.data[11:17] == 'update':
            update_blacklist_mdb(call.from_user.id, config.current_actions[call.from_user.id][1], str(stop_int))
        if call.data[11:17] == 'insert':
            insert_blacklist_mdb(call.from_user.id, config.current_actions[call.from_user.id][1], str(stop_int), 'from_bot')
        
        log_action('ban_number_accept',call.from_user.id, 'номер =: {!s} заблокирован до: {!s}'
                  .format(config.current_actions[call.from_user.id][1], stop_int))
        bot.edit_message_text('Готово. Номер xxxxx' + config.current_actions[call.from_user.id][1][5:] +
                              ' заблокирован до: ' + str(stop_int), call.from_user.id, call.message.message_id)
        markup = types.InlineKeyboardMarkup()
        row = []
        row.append(types.InlineKeyboardButton("Отменить", callback_data="ban-cancel-"+str(call.from_user.id)+'-'+str(number)+'-'+str(stop_int)))
        markup.row(*row)
        bot.send_message(config.HELPER_LOG_CHANNEL,\
                        '\U0000260E #pbx #pbxban\n'+
                        '<b>Заблокирован номер: '+str(number)[-5:]+'</b>'+'\n'+
                        'Пользователь: '+config.ldap_mass[int(call.from_user.id)]['CanonicalName']+'\n'+
                        'AD: '+config.ldap_mass[int(call.from_user.id)]['sAMAccountName']+'\n'+
                        'Telegram: @'+str(call.from_user.username)+'\n'+
                        'Окончание блокировки: '+str(stop_int)+'\n'+'\n'+
                        'Обоснование: '+'\n'+str(justification)+'\n'+'\n'+
                        str(datetime.datetime.strftime(datetime.datetime.now(), "%d %B %Y г. %H:%M:%S")),
                        parse_mode="HTML",
                        reply_markup=markup,
                        )

        clear_vars(call.from_user.id)
        return

    if call.data[18:] == 'нет':
        log_action('ban_number_accept',call.from_user.id, 'номер =: {!s} отмена'.format(config.current_actions[call.from_user.id][1]))
        bot.edit_message_text('Отмена операции.', call.from_user.id, call.message.message_id)
        ban_send_log('#pbx #pbxban','Отмена попытки блокировки номера: '+str(config.current_actions[call.from_user.id][1])[-5:],call)
        clear_vars(call.from_user.id)
        return

def ban_number_cancel(call):
    cancel = call.data.split('-')
    text = '\U0000260E #pbx #pbxban\n'+\
           '<b>Блокировка отменена.</b>'+'\n'+\
           str(datetime.datetime.strftime(datetime.datetime.now(), "%d %B %Y г. %H:%M:%S"))+'\n'+\
           'Пользователь: '+config.ldap_mass[int(call.from_user.id)]['CanonicalName']+'\n'+\
           'AD: '+config.ldap_mass[int(call.from_user.id)]['sAMAccountName']+'\n'+\
           'Telegram: @'+str(call.from_user.username)+'\n'+\
           '-------------------'+'\n'+\
           str(call.message.text.replace('\U0000260E #pbx #pbxban\n',''))
    try:
        update_blacklist_mdb(call.from_user.id, cancel[3], str(datetime.datetime.now()))    
    except:
        return
    log_action('ban_number_cancel',call.from_user.id, 'номер =: {!s} блокировка отменена: {!s}'
                  .format(cancel[3], datetime.datetime.now()))
    bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id, text=text,parse_mode="HTML",)
    bot.send_message(int(cancel[2]), 'Блокировка номера xxxxx'+str(cancel[3][5:])+' отменена @'+str(call.from_user.username))
    return

def ban_send_log(tags,func,m):
    bot.send_message(config.HELPER_LOG_CHANNEL,\
                    '\U0000260E '+tags+'\n'+
                    '<b>'+func+'</b>'+'\n'+
                    'Пользователь: '+config.ldap_mass[int(m.from_user.id)]['CanonicalName']+'\n'+
                    'AD: '+config.ldap_mass[int(m.from_user.id)]['sAMAccountName']+'\n'+
                    'Telegram: @'+str(m.from_user.username)+'\n'+'\n'+
                    str(datetime.datetime.strftime(datetime.datetime.now(), "%d %B %Y г. %H:%M:%S")),
                    parse_mode="HTML",
                    )












