#!/usr/bin/env python3.5
import telebot
from telebot import types
#конфиг и переменные
import config
bot = telebot.TeleBot(config.TOKEN)
import datetime
import time
import pymysql as mdb
import cherrypy
import logging

#конфиг и переменные
#import config

#модули
import ah_date_time as date_time
import ah_decrypt_number as decrypt_num
import ah_blacklist as blacklist
import ah_download_record as get_rec
import ah_abandon_agent as abandon_agent
import ah_yandex as yandex

from ah_ldap import ldap_main as ldap_search
from ah_func import isint,clear_vars,log_access,log_action,log_error,last_inline_clear
from ah_queues import queue_get_num,queue_start,queue_remove_accept 

#bot = telebot.TeleBot(config.TOKEN)

# обработка команд
@bot.message_handler(commands=['start', 'stop'])
def commands(m):
    if not ldap_search(m.chat.id,config.ldap_all):
        log_access('command',m.from_user.id,'запрещен')    
        markup = types.ReplyKeyboardRemove(selective=False)
        bot.send_message(m.chat.id, "We require more vespene gas!!!", reply_markup=markup)
        return

    try:
        last_inline_clear(m.chat.id)
    except:
        pass

    if m.text == '/start':
        buttons = ldap_search(m.chat.id,['list_buttons'])
        log_action('command',m.from_user.id,'ввел команду =: {!s}.'.format(m.text))
        if not buttons:    
            bot.send_message(m.chat.id, "Ошибка данных, обратитесь к системному администратору") 
            return

        keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
        keyboard.add(*[types.KeyboardButton(name) for name in buttons])
        keyboard.add(types.KeyboardButton('Отмена'))
        msg = bot.send_message(m.chat.id, 'Выберите нужный вам пункт меню:', reply_markup=keyboard)
        clear_vars(m.chat.id)
        return
        
    if m.text == '/stop':
        log_action('command',m.from_user.id,'ввел команду =: {!s}.'.format(m.text))
        clear_vars(m.chat.id)
        markup = types.ReplyKeyboardRemove(selective=False)
        bot.send_message(m.chat.id, "GB GG GH", reply_markup=markup)

# обработка собщений чата
@bot.message_handler(func=lambda message: True, content_types=["text"])
def message(m):
    try:
        last_inline_clear(m.chat.id)
    except:
        pass
    if m.text == 'раскрыть номер':
        if not ldap_search(m.chat.id,config.ldap_number_disclosure):
            log_access('[text] =: {!s}'.format(m.text),m.from_user.id,'запрещен')
            return
        log_action('text',m.from_user.id,'ввел команду =: {!s}.'.format(m.text))

        clear_vars(m.chat.id)
        config.current_actions[m.chat.id] = ('get_num','')
        date_time.date_time_type(m.chat.id)

    elif m.text == 'добавить в бан':
        if not ldap_search(m.chat.id,config.ldap_blacklist_add):
            log_access('[text] =: {!s}'.format(m.text),m.from_user.id,'запрещен')
            return 
        log_action('text',m.from_user.id,'ввел команду =: {!s}.'.format(m.text))

        clear_vars(m.chat.id)
        config.current_actions[m.chat.id] = ('ban_num','')
        date_time.date_time_type_later(m.chat.id)
    
    elif m.text == 'выгрузить записи':
        if not ldap_search(m.chat.id,config.ldap_records_download):
            log_access('[text] =: {!s}'.format(m.text),m.from_user.id,'запрещен')
            return
        log_action('text',m.from_user.id,'ввел команду =: {!s}.'.format(m.text))

        clear_vars(m.chat.id)
        config.current_actions[m.chat.id] = ('download_rec','')
        date_time.date_time_type(m.chat.id)

    elif m.text == 'кто пропустил':
        if not ldap_search(m.chat.id,config.ldap_abandon_agent):
            log_access('[text] =: {!s}'.format(m.text),m.from_user.id,'запрещен')
            return
        log_action('text',m.from_user.id,'ввел команду =: {!s}.'.format(m.text))

        clear_vars(m.chat.id)
        config.current_actions[m.chat.id] = ('abandon_agent','')
        abandon_agent.abandon_agent_get_num(m.chat.id)        
    
    elif m.text == 'проверить очереди':
        if not ldap_search(m.chat.id,config.ldap_queue_remove_all):
            log_access('[text] =: {!s}'.format(m.text),m.from_user.id,'запрещен')
            return
        log_action('text',m.from_user.id,'ввел команду =: {!s}.'.format(m.text))

        clear_vars(m.chat.id)
        config.current_actions[m.chat.id] = ('queue_remove_all','')
        queue_get_num(m.chat.id)

    elif m.text == 'yandex map':
        if not ldap_search(m.chat.id,config.ldap_yandex_map_check):
            log_access('[text] =: {!s}'.format(m.text),m.from_user.id,'запрещен')
            return
        log_action('text',m.from_user.id,'ввел команду =: {!s}.'.format(m.text))
        
        clear_vars(m.chat.id)
        config.current_actions[m.chat.id] = ('yandex_map','')
        yandex.ya_start(m.chat.id)

    elif m.text == 'Отмена':
        if not ldap_search(m.chat.id,config.ldap_all):
            log_access('[text] =: {!s}'.format(m.text),m.from_user.id,'запрещен')
            return
        log_action('text',m.from_user.id,'ввел команду =: {!s}.'.format(m.text))

        clear_vars(m.chat.id)

    elif isint(m.text):
        if not ldap_search(m.chat.id,config.ldap_all):
            log_access('[digits] =: {!s}'.format(m.text),m.from_user.id,'запрещен')
            return
        try:
            if config.current_actions[m.chat.id][0] == 'get_num':
                if config.current_actions[m.chat.id][1] == 'interval_set':
                    log_action('digits',m.from_user.id,'раскрыть номер =: {!s}.'.format(m.text))   
                    date_time.calendar_set_interval(m)

            elif config.current_actions[m.chat.id][0] == 'ban_num':
                if config.current_actions[m.chat.id][1] == 'interval_set':
                    log_action('digits',m.from_user.id,'забанить номер =: {!s}.'.format(m.text))
                    date_time.calendar_set_interval(m)

            elif config.current_actions[m.chat.id][0] == 'download_rec': 
                if config.current_actions[m.chat.id][1] == 'interval_set':
                    log_action('digits',m.from_user.id,'выгрузить записи =: {!s}.'.format(m.text))
                    date_time.calendar_set_interval(m)

            elif config.current_actions[m.chat.id][0] == 'abandon_agent':
                log_action('digits',m.from_user.id,'кто пропустил =: {!s}.'.format(m.text))    
                config.current_actions[m.chat.id] = (config.current_actions[m.chat.id][0],m.text)
                abandon_agent.abandon_agent_find_sql(m.chat.id)
            elif config.current_actions[m.chat.id][0] == 'queue_remove_all': 
                log_action('digits',m.from_user.id,'убрать с очереди =: {!s}.'.format(m.text))
                config.current_actions[m.chat.id] = (config.current_actions[m.chat.id][0],m.text) 
                queue_start(m.chat.id)

            else:
                log_action('digits',m.from_user.id,' ввел цифры =: {!s} нет действия.'.format(m.text))

        except Exception as e:
            log_error('text',e)
            bot.send_message(m.chat.id, 'пожалуйста введите /start и попробуйте снова')
            pass
    elif config.current_actions[m.chat.id][0] == 'ban_num':
        if isint(config.current_actions[m.chat.id][1]):
            log_action('text',m.from_user.id,'ввод обоснования')
            blacklist.ban_number_justification(m.chat.id, m.text)
    else:
        log_error('[text] =: {!s}'.format(m.text),'не распознан.')

#####################
##обработка ответов##
#####################

# игнорируемые
@bot.callback_query_handler(func=lambda call: call.data[0:13] == 'ignore')
def ignor_inline(call):
    bot.answer_callback_query(call.id, text="")

# выбор даты.
@bot.callback_query_handler(func=lambda call: call.data[0:14] == 'calendar-date-')
def change_month(call):
    date_time.date_action(call)

# выбор времени
@bot.callback_query_handler(func=lambda call: call.data[0:15] == 'calendar-times-')
def get_time(call):
    date_time.time_action(call)

# выбор пропущенного звонка
@bot.callback_query_handler(func=lambda call: call.data[0:14] == 'abandon-agent-')
def get_abandon(call):
    last_inline_clear(call.message.chat.id)
    abandon_agent.abandon_agent_find_ssh(call.message.chat.id,call.data[14:])

# тип выбора даты
@bot.callback_query_handler(func=lambda call: call.data[0:13] == 'calendar-type')
def calendar_type(call):
    action_stage = config.current_actions.get(call.from_user.id)
    config.current_actions[call.from_user.id] = (action_stage[0],call.data[14:])
    date_time.calendar_set_interval(call) 
    return

# подтверждение блокировки
@bot.callback_query_handler(func=lambda call: call.data[0:11] == 'ban-accept-')
def ban_aсcept(call):
    bot.answer_callback_query(callback_query_id=call.id, show_alert=False, text="")
    blacklist.ban_number_accept(call)    

# отмена блокировки
@bot.callback_query_handler(func=lambda call: call.data[0:11] == 'ban-cancel-')
def ban_cancel(call):
    if not ldap_search(call.from_user.id,config.ldap_blacklist_add):
            log_access('[ban_cancel] =: {!s}'.format('отмена блокировки'),m.from_user.id,'запрещен')
            return
    bot.answer_callback_query(callback_query_id=call.id, show_alert=False, text="")
    blacklist.ban_number_cancel(call)

#подтверждение выбросить из очереди
@bot.callback_query_handler(func=lambda call: call.data[0:12] == 'queue-accept')
def queue_accept(call):
    queue_remove_accept(call)

# выбор действия яндекс карт
@bot.callback_query_handler(func=lambda call: call.data[0:7] == 'yandex-')
def yandex_action(call):
    action_stage = config.current_actions.get(call.from_user.id)
    config.current_actions[call.from_user.id] = (action_stage[0],call.data[7:])
    bot.edit_message_text('Выбрано действие: ' + config.current_actions[call.from_user.id][1]
                                                , call.from_user.id, call.message.message_id)
    yandex.ya_action(call.from_user.id)

class WebhookServer(object):
    @cherrypy.expose
    def index(self):
        if 'content-length' in cherrypy.request.headers and \
                'content-type' in cherrypy.request.headers and \
                cherrypy.request.headers['content-type'] == 'application/json':
            length = int(cherrypy.request.headers['content-length'])
            json_string = cherrypy.request.body.read(length).decode("utf-8")
            update = telebot.types.Update.de_json(json_string)
            # Эта функция обеспечивает проверку входящего сообщения
            bot.process_new_updates([update])
            return ''
        else:
            raise cherrypy.HTTPError(403)


if __name__ == '__main__':
    # Настраиваем логгер
    logging.basicConfig(format='[%(asctime)s] %(filename)s:%(lineno)d %(levelname)s - %(message)s', level=logging.INFO,
                        filename='log_bot.log', datefmt='%d.%m.%Y %H:%M:%S')
    telebot.logger.setLevel(logging.INFO)
    logging.getLogger("paramiko").setLevel(logging.WARNING)

    bot.remove_webhook()
    bot.set_webhook(url=config.WEBHOOK_URL_BASE + config.WEBHOOK_URL_PATH)

    # Указываем настройки сервера CherryPy
    cherrypy.config.update({
        'server.socket_host': config.WEBHOOK_LISTEN,
        'server.socket_port': config.WEBHOOK_PORT,
    })

    # Собственно, запуск!
    cherrypy.quickstart(WebhookServer(), config.WEBHOOK_URL_PATH, {'/': {}})

