import calendar
import datetime
import telebot
from telebot import types
from ah_bot import bot

import config

import ah_decrypt_number as decrypt_num
from ah_func import clear_vars, log_action,last_inline_set
import ah_download_record as get_rec
import ah_blacklist as blacklist
from ah_ldap import ldap_main as ldap_search

#bot = telebot.TeleBot(config.TOKEN)

RU_MONTH = {
    1: 'январь',
    2: 'февраль',
    3: 'март',
    4: 'апрель',
    5: 'май',
    6: 'июнь',
    7: 'июль',
    8: 'август',
    9: 'сент.',
    10: 'октябрь',
    11: 'ноябрь',
    12: 'декабрь'
}
#создание инлайн календаря
def create_calendar(year, month):
    markup = types.InlineKeyboardMarkup()
    # First row - Month and Year
    row = []
    row.append(types.InlineKeyboardButton("<", callback_data="calendar-date-month-previous"))
    row.append(types.InlineKeyboardButton(str(RU_MONTH[month]), callback_data="ignore"))
    row.append(types.InlineKeyboardButton(str(year), callback_data="ignore"))
    row.append(types.InlineKeyboardButton(">", callback_data="calendar-date-month-next"))
    markup.row(*row)
    # Second row - Week Days
    week_days = ["Пн", "Вт", "Ср", "Чт", "Пт", "Сб", "Вс"]
    row = []
    for day in week_days:
        row.append(types.InlineKeyboardButton(day, callback_data="ignore"))
    markup.row(*row)

    my_calendar = calendar.monthcalendar(year, month)
    for week in my_calendar:
        row = []
        for day in week:
            if (day == 0):
                row.append(types.InlineKeyboardButton(" ", callback_data="ignore"))
            else:
                row.append(types.InlineKeyboardButton(str(day), callback_data="calendar-date-day-" + str(day)))
        markup.row(*row)
    return markup

#создание инлайн часов
def create_clock(hour, minut):
    markup = types.InlineKeyboardMarkup()

    row = []
    row.append(types.InlineKeyboardButton('^', callback_data="calendar-times-hour-up"))
    row.append(types.InlineKeyboardButton('^', callback_data="calendar-times-minute-up"))
    markup.row(*row)

    row = []
    times_of_time = [str(hour), ":", str(minut)]
    for times in times_of_time:
        row.append(types.InlineKeyboardButton(times, callback_data="ignore"))
    markup.row(*row)

    row = []
    row.append(types.InlineKeyboardButton('v', callback_data="calendar-times-hour-down"))
    row.append(types.InlineKeyboardButton('v', callback_data="calendar-times-minute-down"))
    markup.row(*row)

    row = []
    row.append(types.InlineKeyboardButton("выбрать", callback_data="calendar-times-select"))
    markup.row(*row)
    return markup

def date_time_type(chat_id):
    markup = types.InlineKeyboardMarkup()
    row = []
    row.append(types.InlineKeyboardButton("за 1 час", callback_data="calendar-type-hour"))
    row.append(types.InlineKeyboardButton("за 1 день", callback_data="calendar-type-day"))
    row.append(types.InlineKeyboardButton("за 3 дня", callback_data="calendar-type-3day"))
    markup.row(*row)
    row = []
    row.append(types.InlineKeyboardButton("календарь", callback_data="calendar-type-calendar"))
    markup.row(*row)
    inl = bot.send_message(chat_id, 'Выберите нужный интервал:', reply_markup=markup)
    last_inline_set(chat_id,inl.message_id)
    return

def date_time_type_later(chat_id):
    markup = types.InlineKeyboardMarkup()
    row = []
    row.append(types.InlineKeyboardButton("на 1 час", callback_data="calendar-type-to_1h"))
    if ldap_search(chat_id,config.ldap_admins):
        row.append(types.InlineKeyboardButton("на 3 часа", callback_data="calendar-type-to_3h"))
        row.append(types.InlineKeyboardButton("на 12 часов", callback_data="calendar-type-to_12h"))
    markup.row(*row)
    if ldap_search(chat_id,config.ldap_admins):
        row = []
        row.append(types.InlineKeyboardButton("на 1 день", callback_data="calendar-type-to_1d"))
        row.append(types.InlineKeyboardButton("на 3 дня", callback_data="calendar-type-to_3d"))
        row.append(types.InlineKeyboardButton("на 7 дней", callback_data="calendar-type-to_7d"))
        markup.row(*row)
        row = []
        row.append(types.InlineKeyboardButton("календарь", callback_data="calendar-type-calendar"))
        markup.row(*row)
        row = []
        row.append(types.InlineKeyboardButton("навсегда", callback_data="calendar-type-to_forever"))
        markup.row(*row)

    inl = bot.send_message(chat_id, 'Выберите нужный интервал:', reply_markup=markup)
    last_inline_set(chat_id,inl.message_id)
    return

def calendar_set_interval(call):
    chat_id = call.from_user.id
    action_stage = config.current_actions.get(chat_id)

    now = datetime.datetime.now()
    past = False
    later = False

    if action_stage[1] == 'calendar':
        config.current_actions[chat_id] = (action_stage[0],'date-start')
        bot.delete_message(chat_id,call.message.message_id)
        date_pick(call.from_user.id)
        return

    elif action_stage[1] == 'hour':
        past = (datetime.datetime.now() - datetime.timedelta(hours=1))
        config.current_actions[chat_id] = (action_stage[0],'interval_set')
    elif action_stage[1] == 'day':
        past = (datetime.datetime.now() - datetime.timedelta(days=1))
        config.current_actions[chat_id] = (action_stage[0],'interval_set')
    elif action_stage[1] == '3day':
        past = (datetime.datetime.now() - datetime.timedelta(days=3))
        config.current_actions[chat_id] = (action_stage[0],'interval_set')
    
    elif action_stage[1] == 'to_1h':
        later = (datetime.datetime.now() + datetime.timedelta(hours=1))
        config.current_actions[chat_id] = (action_stage[0],'interval_set')
    elif action_stage[1] == 'to_3h':
        later = (datetime.datetime.now() + datetime.timedelta(hours=3))
        config.current_actions[chat_id] = (action_stage[0],'interval_set')
    elif action_stage[1] == 'to_12h':
        later = (datetime.datetime.now() + datetime.timedelta(hours=12))
        config.current_actions[chat_id] = (action_stage[0],'interval_set')
    elif action_stage[1] == 'to_1d':
        later = (datetime.datetime.now() + datetime.timedelta(days=1))
        config.current_actions[chat_id] = (action_stage[0],'interval_set')
    elif action_stage[1] == 'to_3d':
        later = (datetime.datetime.now() + datetime.timedelta(days=3))
        config.current_actions[chat_id] = (action_stage[0],'interval_set')
    elif action_stage[1] == 'to_7d':
        later = (datetime.datetime.now() + datetime.timedelta(days=7))
        config.current_actions[chat_id] = (action_stage[0],'interval_set')
    elif action_stage[1] == 'to_forever':
        later = datetime.datetime(2060,1,1,0,0,0)
        config.current_actions[chat_id] = (action_stage[0],'interval_set')

    if past:
        config.current_shown_datetime[str(chat_id)+'-date-start'] = (past.year,past.month,past.day)
        config.current_shown_datetime[str(chat_id)+'-date-stop'] = (now.year,now.month,now.day)
        config.current_shown_datetime[str(chat_id)+'-time-start'] = (past.hour,past.minute)
        config.current_shown_datetime[str(chat_id)+'-time-stop'] = (now.hour,now.minute)
        if action_stage[0] == 'get_num':
            bot.edit_message_text('Введите последние 5 цифр номера:', call.from_user.id, call.message.message_id)
        if action_stage[0] == 'download_rec':
            bot.edit_message_text('Введите 10 значный номер:', call.from_user.id, call.message.message_id)
        return
    if later:
        config.current_shown_datetime[str(chat_id)+'-date-stop'] = (later.year,later.month,later.day)
        config.current_shown_datetime[str(chat_id)+'-time-stop'] = (later.hour,later.minute)
        if action_stage[0] == 'ban_num':
            bot.edit_message_text('введите последние цифры номера(не менее 5 цифр) или номер целиком:', call.from_user.id, call.message.message_id)
        return
    if action_stage[1] == 'interval_set':
    
        #config.current_actions[chat_id] = (action_stage[0],call.text)
        date_stop = config.current_shown_datetime.get(str(chat_id)+'-date-stop')
        time_stop = config.current_shown_datetime.get(str(chat_id)+'-time-stop')
        stop_int = datetime.datetime(date_stop[0],date_stop[1],date_stop[2],time_stop[0],time_stop[1],00)
        if action_stage[0] == 'ban_num':
            #bot.send_message(chat_id,'окончание блокировки: '+str(stop_int))
            blacklist.ban_number_decript(call)
            return

        config.current_actions[chat_id] = (action_stage[0],call.text) 
        date_start = config.current_shown_datetime.get(str(chat_id)+'-date-start')
        time_start = config.current_shown_datetime.get(str(chat_id)+'-time-start')
        start_int = datetime.datetime(date_start[0],date_start[1],date_start[2],time_start[0],time_start[1],00)
        
        bot.send_message(chat_id,'выбран интервал: '+str(start_int)+' - '+str(stop_int))
        delta = stop_int - start_int
        if int(delta.days) > 31:
            bot.send_message(call.from_user.id,'Выбранный интервал слишком велик. разрешено не более 31 дней.') 
            log_action('calendar_set_interval',call.from_user.id,'Выбранный интервал слишком велик и равен: {!s}'.format(delta.days))
            return
        if action_stage[0] == 'get_num':
            decrypt_num.get_num(call,start_int,stop_int)
            return
    
        if action_stage[0] == 'download_rec':
            get_rec.download_record_get(chat_id,start_int,stop_int)
            return
        return


#выбор даты
def date_pick(chat_id):
    action_stage = config.current_actions.get(chat_id)
    if action_stage[0] == 'ban_num':
        if action_stage[1] == 'date-start':
            config.current_shown_datetime[str(chat_id)+'-'+action_stage[1]] = (0, 0, 0)
            config.current_shown_datetime[str(chat_id)+'-'+'time-start'] = (0, 0)
            config.current_actions[chat_id] = (action_stage[0],'date-stop')
            action_stage = config.current_actions.get(chat_id)

    if action_stage[1] == 'date-start':
        mess = "Пожалуйста выбирите начальную дату:"
    else:
        mess = "Пожалуйста выбирите конечную дату:"
    now = datetime.datetime.now()
    config.current_shown_datetime[str(chat_id)+'-'+action_stage[1]] = (now.year, now.month,now.day) 
    markup = create_calendar(now.year, now.month)
    inl = bot.send_message(chat_id, mess, reply_markup=markup)
    last_inline_set(chat_id,inl.message_id)

#действие с датой    
def date_action(call): 
    chat_id = call.message.chat.id
    action_stage = config.current_actions.get(chat_id)
    saved_date = config.current_shown_datetime.get(str(chat_id)+'-'+action_stage[1])
    
    if (saved_date is not None):
        year, month, day = saved_date
        if call.data == 'calendar-date-month-next':
            month += 1
            if month > 12:
                month = 1
                year += 1
        if call.data == 'calendar-date-month-previous':
            month -= 1
            if month < 1:
                month = 12
                year -= 1

        config.current_shown_datetime[str(chat_id)+'-'+action_stage[1]] = (year, month, day)

        if call.data[0:18] == 'calendar-date-day-':
            if action_stage[1] == 'date-start':
                mess = 'начальная дата:'
            else:
                mess = 'конечная дата:'
            
            day = int(call.data[18:])
            config.current_shown_datetime[str(chat_id)+'-'+action_stage[1]] = (year, month, day)
            date = datetime.date(int(saved_date[0]), int(saved_date[1]), int(day))
            bot.edit_message_text(mess + str(date), chat_id, call.message.message_id)
            bot.answer_callback_query(call.id, text="")
            if action_stage[1] == 'date-start':
                config.current_actions[chat_id] = (action_stage[0],'time-start')
                time_pick(chat_id)

            if action_stage[1] == 'date-stop':
                config.current_actions[chat_id] = (action_stage[0],'time-stop')
                time_pick(chat_id)
                return
            return

        if action_stage[1] == 'date-start':
            mess = 'Пожалуйста выбирите начальную дату:'
        else:
            mess = 'Пожалуйста выбирите конечную дату:'

        markup = create_calendar(year, month)
        bot.edit_message_text(mess, call.from_user.id, call.message.message_id,
                              reply_markup=markup)
        bot.answer_callback_query(call.id, text="")
    else:
        # Do something to inform of the error
        pass

def time_pick(chat_id):
    action_stage = config.current_actions.get(chat_id)
    if action_stage[1] == 'time-start':
        mess = "Пожалуйста выбирите начальное время:"
    else:
        mess = "Пожалуйста выбирите конечное время:"
    now = datetime.datetime.now()
    if int(round(now.minute/10.0)*10) > 59:
        minute = 0
    else:
        minute = int(round(now.minute/10.0)*10)
    config.current_shown_datetime[str(chat_id)+'-'+action_stage[1]] = \
                                (now.hour, minute)
    markup = create_clock(now.hour, int(round(now.minute/10.0)*10))
    inl = bot.send_message(chat_id, mess, reply_markup=markup)
    last_inline_set(chat_id,inl.message_id)
def time_action(call):
    chat_id = call.message.chat.id
    action_stage = config.current_actions.get(chat_id)
    saved_time = config.current_shown_datetime.get(str(chat_id)+'-'+action_stage[1])
    if (saved_time is not None):
        hour, minute = saved_time
        if call.data == 'calendar-times-hour-up':
            hour += 1
            if hour > 23:
                hour = 0
        if call.data == 'calendar-times-hour-down':
            hour -= 1
            if hour < 0:
                hour = 23
        if call.data == 'calendar-times-minute-up':
            minute += 10
            if minute > 59:
                minute = 0
        if call.data == 'calendar-times-minute-down':
            minute -= 10
            if minute < 0:
                minute = 59

        if call.data == 'calendar-times-select':
            if action_stage[1] == 'time-start':
                mess = "начальное время:"
            else:
                mess = 'конечное время:'

            times = datetime.time(int(saved_time[0]), int(saved_time[1]), 59)
            bot.edit_message_text(mess + str(times), chat_id, call.message.message_id)
            bot.answer_callback_query(call.id, text="")
            if action_stage[1] == 'time-start':
                config.current_actions[chat_id] = (action_stage[0],'date-stop')
                date_pick(chat_id)
            if action_stage[1] == 'time-stop':
                config.current_actions[chat_id] = (action_stage[0],'interval_set')
                if action_stage[0] == 'get_num':
                    bot.send_message(chat_id, 'введите последние 5 цифр номера:')
                if action_stage[0] == 'download_rec':
                    bot.send_message(chat_id, 'введите 10 значный номер:')        
                if action_stage[0] == 'ban_num':
                    bot.send_message(chat_id, 'введите последние цифры номера(не менее 5 цифр) или номер целиком:')
            return

        if action_stage[1] == 'time-start':
                mess = "Пожалуйста выбирите начальное время:"
        else:
                mess = 'Пожалуйста выбирите конечное время:'

        config.current_shown_datetime[str(chat_id)+'-'+action_stage[1]] = (hour, minute)
        markup = create_clock(hour, minute)
        bot.edit_message_text(mess, call.from_user.id, call.message.message_id,
                              reply_markup=markup)
        bot.answer_callback_query(call.id, text="")





















