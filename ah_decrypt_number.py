import datetime

import telebot
from telebot import types
from ah_bot import bot

from ah_func import log_action, log_error, log_access, clear_vars
import config
import ah_sql 


def get_num(m,start_int,stop_int):
    action_stage = config.current_actions.get(m.chat.id)
    if len(m.text) >= 5 and len(m.text) <=9:
        bot.send_message(m.chat.id, 'ищю номер '+m.text+' в интервале '+\
                            str(start_int)+' - '+str(stop_int))
        log_action('get_num',m.chat.id,'ищю номер {!s} в интервале {!s} - {!s}'.format(m.text,start_int,stop_int))

        results = ah_sql.select_src_mdb(m.chat.id, m.text, str(start_int), str(stop_int))   
        try:
            if len(results) > 1:
                bot.send_message(m.chat.id, 'Найдено более 1 номера, попробуйте заново, указав больше цифр, или выберете меньший интервал')
                send_log('#pbx #pbxdisclosure #pbxdisclosurefailure','Неудачная попытка раскрыть номер: '+str(m.text),m)
                log_action('get_num',m.chat.id,'ошибка найдено номеров: {!s}'.format(len(results)))
                return
        
            elif len(results) == 1:
                for items in results:
                    src = items[0]
                log_action('get_num',m.chat.id,'номер {!s} раскрыт'.format(src))
                bot.send_message(m.chat.id, 'номер - '+ str(src))
                send_log('#pbx #pbxdisclosure #pbxdisclosuresuccess','Раскрыт номер: '+str(src)[-5:],m)

            else:
                log_action('get_num',m.chat.id,'попытка найти номер =: {!s},поиск не дал результатов'.format(m.text))
                bot.send_message(m.chat.id, 'поиск не дал результатов')
                send_log('#pbx #pbxdisclosure #pbxdisclosurefailure','Неудачная попытка раскрыть номер: '+str(m.text),m)
        
        except Exception as Ex:
            log_action('get_num',m.chat.id,'попытка найти номер =: {!s},ошибка =: {!s}'.format(m.text, Ex))
            bot.send_message(m.chat.id, 'поиск не дал результатов')
            send_log('#pbx #pbxdisclosure #pbxdisclosurefailure','Неудачная попытка раскрыть номер: '+str(m.text),m)
    else:
        log_action('get_num',m.chat.id,'неверная длинна =: {!s}'.format(m.text))
        bot.send_message(m.chat.id, 'неверная длинна')
        send_log('#pbx #pbxdisclosure #pbxdisclosurefailure','Неудачная попытка раскрыть номер: '+str(m.text),m)

def send_log(tags,func,m):
    bot.send_message(config.HELPER_LOG_CHANNEL,\
                    '\U0000260E '+tags+'\n'+
                    '<b>'+func+'</b>'+'\n'+
                    'Пользователь: '+config.ldap_mass[int(m.from_user.id)]['CanonicalName']+'\n'+
                    'AD: '+config.ldap_mass[int(m.from_user.id)]['sAMAccountName']+'\n'+
                    'Telegram: @'+str(m.from_user.username)+'\n'+'\n'+
                    str(datetime.datetime.strftime(datetime.datetime.now(), "%d %B %Y г. %H:%M:%S")),
                    parse_mode="HTML",
                    )





