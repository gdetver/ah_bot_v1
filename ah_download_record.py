import datetime
import os
import sys
import paramiko
import zipfile
import xlsxwriter


import telebot
from telebot import types
from ah_bot import bot

import config
import ah_sql
from ah_func import log_action, log_error, log_access,clear_vars


def download_record_get(chat_id,start_int,stop_int):
    action_stage = config.current_actions.get(chat_id)
    if len(action_stage[1]) == 10:
        download_clear_dir()
        number = action_stage[1]
        bot.send_message(chat_id, 'Ищю записи для номера: '+str(number)+' в интервале: '+str(start_int)+' - '+str(stop_int))
       
        log_action('download_record_get',chat_id, 'поиск записей для номера: {!s} в интервале: {!s} - {!s}.'\
                    .format(number,start_int,stop_int))
 
        result = ah_sql.select_rec_mdb(chat_id,number,str(start_int),str(stop_int))
        log_action('download_record_get',chat_id, 'количество звонков: {!s}.'\
                    .format(len(result)))
        
        print(datetime.datetime.now())
        rec_list = download_check_count(result)
        print(datetime.datetime.now())
        
        log_action('download_record_get',chat_id, 'количество файлов записей: {!s}.'\
                    .format(len(rec_list)))
    
        bot.send_message(chat_id, 'количество звонков: '+str(len(result))+', количество записей: '+str(len(rec_list)))
        
        
        if int(len(rec_list)) == 0:
            bot.send_message(chat_id, 'Записи не найдены')
            log_action('download_record_get',chat_id, 'Записи не найдены.')
            send_log('#pbx #pbxrecords #pbxrecordsfailed',
                     'для номера '+str(number)[-5:]+' записи не найдены.',chat_id)
        
        elif int(len(rec_list)) > 0 and int(len(rec_list)) <=20:
            bot.send_message(chat_id, 'качаю записи.')
            list = download_rsync(rec_list)
            log_action('download_record_get',chat_id, 'скачано записей: {!s}.'\
                        .format(len(list)))
            download_delailing(result)
            for folder, subfolders, files in os.walk(config.REC_DIR):
                for file in files:
                    doc = open(config.REC_DIR+file,'rb')
                    if file.endswith('.mp3') or file.endswith('.wav'):
                        bot.send_audio(chat_id, doc)
                    else:
                        bot.send_document(chat_id, doc)
                    os.unlink(config.REC_DIR+file)
                    doc.close()

            bot.send_message(chat_id, 'выгружено записей: '+str(len(list)))    
            send_log('#pbx #pbxrecords #pbxrecordssuccess',
                     'для номера '+str(number)[-5:]+' выгружено записей: ' + str(len(list)),chat_id)

        elif int(len(rec_list)) > 20 and int(len(rec_list)) <=100:
            bot.send_message(chat_id, 'качаю записи.')
            list = download_rsync(rec_list)
            download_zip('.mp3', number)
            log_action('download_record_get',chat_id, 'скачано записей: {!s}.'\
                        .format(len(list)))
            
            download_delailing(result)
            download_zip('.xls',number)
            doc = open(config.REC_DIR+str(number[4:10])+".zip", 'rb')
            bot.send_document(chat_id, doc)
            os.unlink(config.REC_DIR+str(number[4:10])+".zip")
            doc.close()
            bot.send_message(chat_id, 'выгружено записей: '+str(len(list)))
            send_log('#pbx #pbxrecords #pbxrecordssuccess',
                     'для номера '+str(number)[-5:]+' выгружено записей: ' + str(len(list)),chat_id)
        elif int(len(rec_list)) > 100:
            bot.send_message(chat_id, 'количество записей слишком велико: '+str(len(rec_list))+\
                        ', обратитесь к системному администратору, либо сократите диапазон')
            log_action('download_record_get',chat_id, 'количество записей слишком велико: {!s}.'\
                        .format(len(rec_list)))
            send_log('#pbx #pbxrecords #pbxrecordsfailed',
                     'слишком много записей для номера '+str(number)[-5:]+', количество записей: ' + str(len(rec_list)),chat_id)

        download_clear_dir()
        return
    else:
        log_action('download_record_get',chat_id,'неверная длинна =: {!s}'.format(action_stage[1])) 
        bot.send_message(chat_id, 'неверная длинна номера.')
        send_log('#pbx #pbxrecords #pbxrecordsfailed',
                     'неверная длинна номера '+str(action_stage[1]),chat_id)
        


def ssh_client(command):
    try:
        client = paramiko.SSHClient()
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        client.connect(hostname=config.SSH_DMP_HOST, username=config.SSH_DMP_USER, password=config.SSH_DMP_SECRET, port=config.SSH_DMP_PORT)
        stdin, stdout, stderr = client.exec_command(command)
        data = stdout.read() + stderr.read()
        ssh_err = stderr.read()
        ssh_out = stdout.read()
        client.close()
        return data, ssh_err, ssh_out
    except Exception as eX:
        log_error('ssh_client',eX)

def download_clear_dir():
    folder = config.REC_DIR
    for the_file in os.listdir(folder):
        file_path = os.path.join(folder, the_file)
        try:
            if os.path.isfile(file_path):
                os.unlink(file_path)
        except Exception as e:
            print(e)

def download_check_count(result):
    rec_list = []
    rec_dir = []
    rec_dir_clear = []
    rec_mask_dict = {}

    # составляем список папок
    rec_dir = [item[0][0:10] for item in result]
    [rec_dir_clear.append(x) for x in rec_dir if x not in rec_dir_clear]
    
    # цикл обхода папок    
    for key in rec_dir_clear:
        # цикл обхода списка записей, формируем строку креп для конкретной папки
        for item in result:
            if item[0][0:10] == key:
                if not key in rec_mask_dict:
                    rec_mask_dict[key] = [item[0]]           
                else:
                    rec_mask_dict[key].append(item[0])
        # find по папке
        rec_mask = '\|'.join(rec_mask_dict[key])
        data, stdout, stderr =  ssh_client("find /mnt/asterisk/records/"+key+" | grep -e '"+rec_mask+"'") 
        tmp = data.decode().split('\n')
        # формируем кортеж для скачивание
        for x in tmp:
            if x == '':
                continue
            rec_list.append(x)

    return rec_list

def update_progress(i,name):
    sys.stdout.write(('#'*i)+(''*(100-i))+("\r"+name+" [ %d"%i+"% ] "))

def download_rsync(rec_list):
    count = 1
    for item in rec_list:
        data, stdout, stderr =  ssh_client("rsync -avz "+item+" ahbot@10.10.9.79:/home/ahbot/tmp/records/")
        i=round(count/len(rec_list)*100)
        update_progress(i,'download')
        count = count + 1
    print()
    return os.listdir(config.REC_DIR)

def download_zip(mask,number):
    list = os.listdir(config.REC_DIR)
    count = 1
    if int(len(list)) > 0 and int(len(list)) <=100:
        z = zipfile.ZipFile(config.REC_DIR+str(number[4:10])+".zip", 'a')
        for folder, subfolders, files in os.walk(config.REC_DIR):
            for file in files:
                if file.endswith(mask):
                    i=round(count/len(list)*100)
                    update_progress(i,'ZIPing')
                    count = count+1
                    z.write(os.path.join(folder, file), file, compress_type = zipfile.ZIP_DEFLATED)
                    os.unlink(folder+file)
        z.close()
        print()
    return

def download_delailing(result):
    data = [[item[1],str(item[2]),str(item[3]),str(item[4]),str(item[5]),str(item[6]),str(item[7]),str(item[8]),str(item[0])] for item in result]
    wb = xlsxwriter.Workbook(config.REC_DIR+'detailing.xls')
    ws = wb.add_worksheet()
    date_format = wb.add_format({'num_format': 'yyyy.mm.dd hh:mm:ss', 'align': 'fill'})
    cell_format = wb.add_format()
    cell_format.set_align('fill')
    ws.add_table('A1:I'+str((len(result)+1)),
                {'data': data,
                #'style': 'Table Style Light 16',
                'columns': [{'header': 'дата\время', 'format':date_format},
                            {'header': 'источник'},
                            {'header': 'назначение'},
                            {'header': 'доп номер'},
                            {'header': 'Агент'},
                            {'header': 'ID агента'},
                            {'header': 'длительность'},
                            {'header': 'Статус'},
                            {'header': 'запись'},
                            ]
                })
    wb.close()
    return

def send_log(tags,func,chat_id):
    bot.send_message(config.HELPER_LOG_CHANNEL,\
                    '\U0000260E '+tags+'\n'+
                    '<b>'+func+'</b>'+'\n'+
                    'Пользователь: '+config.ldap_mass[int(chat_id)]['CanonicalName']+'\n'+
                    'AD: '+config.ldap_mass[int(chat_id)]['sAMAccountName']+'\n'+'\n'+
                    str(datetime.datetime.strftime(datetime.datetime.now(), "%d %B %Y г. %H:%M:%S")),
                    parse_mode="HTML",
                    )

