import config
import logging
import telebot
from ah_bot import bot

#bot = telebot.TeleBot(config.TOKEN)

# проверка на число
def isint(s):
    try:
        int(s)
        return True
    except ValueError:
        return False

#очистка переменных окружения пользователя
def clear_vars(chat_id):
    list_keys = list(config.current_actions.keys())
    for key in list_keys:
        if str(key).split('-')[0] == str(chat_id):
            del config.current_actions[key]
    
    list_keys = list(config.current_shown_datetime.keys())
    for key in list_keys:
        if str(key).split('-')[0] == str(chat_id):
            del config.current_shown_datetime[key]
    return

#Логирование
def log_action(program,user,action):
    logging.info('[{!s}][[user_id] =: {!s}, [sAMAccountName] =: {!s}, [CN] =: {!s}] => [{!s}]'\
                    .format(program,\
                            user,\
                            config.ldap_mass[int(user)]['sAMAccountName'],\
                            config.ldap_mass[int(user)]['CanonicalName'],\
                            action
                            ))
    return

def log_access(program,user,status):
    logging.info('[{!s}][[user_id] =: {!s}] => [Доступ {!s}]'\
                .format(program,\
                        user,\
                        status
                        ))
    return
    
def log_error(program,eX):
    logging.error('[{!s}] => [ошибка =: {!s}]'\
                .format(program,\
                        eX
                        ))
    return    
  
def last_inline_set(chat_id, mess_id):
    config.last_inline[chat_id] = mess_id
    return             

def last_inline_clear(chat_id):
    bot.delete_message(chat_id,config.last_inline[chat_id])
    return





 
