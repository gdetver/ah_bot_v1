#!/usr/bin/env python3.5
import ldap
import config
import logging

#основная функция поиска id среди номеров факсов в АД
def ldap_main(chat_id,ldap_group):
    ldap_conn = ldap_connect()
    if ldap_conn != False:
        if ldap_group[0] == 'list_buttons':
            buttons = ldap_buttons_group(ldap_conn,chat_id)
            return buttons

        elif ldap_group[0] == '*':
            groups = ''
        else:    
            groups = '(|'
            for group in ldap_group:
                groups = groups+'(memberOf=CN={0},OU=Astbot,OU=Groups,OU=Gazelkin,DC=gazelkin,DC=local)'.format(str(group))
            groups = groups+')'

        logging.info('[ldap_main] =: "{!s}".'.format('Searching..'))

        if ldap_search_fax_group(ldap_conn,groups,chat_id):
            return True
        
#создание и проверка подключения
def ldap_connect():
    try:
        con = ldap.open(config.LDAP_HOST)
        con.protocol_version = ldap.VERSION3
        con.simple_bind_s(config.LDAP_USER, config.LDAP_PASS)
        logging.info('[ldap_connect] =: "{!s}".'.format('Successfully bound to server'))
        return con
    except Exception as error_message:
        logging.info('[ldap_connect] =: "{!s}".'.format(str(error_message)))
        return False

#поиск пользователя с полем факс = id в группах бота
def ldap_search_fax_group(ldap_conn,groups,chat_id):
#    base = "OU=Users Accounts,OU=Gazelkin,DC=gazelkin,DC=local"
    base = "OU=Gazelkin,DC=gazelkin,DC=local"
    scope = ldap.SCOPE_SUBTREE
    searchFilter = '(&(objectclass=user)(objectcategory=person)\
                    (!(userAccountControl:1.2.840.113556.1.4.803:=2))'+groups+'\
                    (|(facsimileTelephoneNumber={0})(otherFacsimileTelephoneNumber={0})))'\
                    .format(str(chat_id))
    searchAttribute = ['sAMAccountName','cn','facsimileTelephoneNumber','otherFacsimileTelephoneNumber']
    ldap_result_id = ldap_conn.search(base, scope, searchFilter, searchAttribute)
    count = 0
    
    try:
        while 1:
            result_type, result_data = ldap_conn.result(ldap_result_id, 0)
            if (result_data == []):
                break
            else:
                count = count + 1
                if result_type == ldap.RES_SEARCH_ENTRY:
                    for result in result_data:
                        if count == 1:
                            sAMAccountName = result[1]['sAMAccountName'][0].decode("utf-8")
                            CanonicalName = result[1]['cn'][0].decode("utf-8")
        ldap_conn.unbind_s()


        if count == 0:
            return False
        elif count == 1:
            config.ldap_mass[chat_id] = {'sAMAccountName':sAMAccountName,'CanonicalName':CanonicalName} 
            return True
        elif count > 1:
            config.ldap_mass[chat_id] = {'sAMAccountName':'Multy','CanonicalName':'Многопользовательский акк'}
            return True
        else:
            return False
        
    except Exception as error_message:
        logging.info('[ldap_search_fax_group] =: "{!s}".'.format(str(error_message)))
        ldap_conn.unbind_s()
        return False

#поиск групп бота по id телеги, возврат кортежа для кнопок
def ldap_buttons_group(ldap_conn,chat_id):
    base = "OU=Gazelkin,DC=gazelkin,DC=local"

#    base = "OU=Users Accounts,OU=Gazelkin,DC=gazelkin,DC=local"
    scope = ldap.SCOPE_SUBTREE
    searchFilter = '(&(objectclass=user)(objectcategory=person)\
                    (!(userAccountControl:1.2.840.113556.1.4.803:=2))\
                    (|(facsimileTelephoneNumber={0})(otherFacsimileTelephoneNumber={0})))'\
                    .format(str(chat_id))
    searchAttribute = ['memberOf',]
    ldap_result_id = ldap_conn.search(base, scope, searchFilter, searchAttribute)
    count = 0
    list_buttons = []
    try:
        while 1:
            result_type, result_data = ldap_conn.result(ldap_result_id, 0)
            if (result_data == []):
                break
            else:
                if result_type == ldap.RES_SEARCH_ENTRY:
                    for gr in result_data[0][1]['memberOf']:
                        gr = gr.decode("utf-8").split(',',1)[0].split('=')[1]
                        if gr[0:6] == 'Astbot':
                            try:
                                if gr == 'Astbot_admins':
                                    list_buttons = config.ldap_buttons[gr]
                                    count = count + 1
                                    break
                                list_buttons.extend(config.ldap_buttons[gr])
                                count = count + 1

                            except Exception as eX:
                                logging.info('[ldap_button_group] =: "{!s}" don\'t exist.'.format(str(eX)))
        ldap_conn.unbind_s()
        if count == 0:
            return False
        else:
            return list_buttons
    except Exception as eX:
        logging.info("Couldn't process =: {!s}.".format(str(ex)))
        ldap_conn.unbind_s()
        return False



