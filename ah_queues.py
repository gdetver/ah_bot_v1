#!/usr/bin/python
#coding=utf-8
import datetime
import requests
import os
import sys

import telebot
from telebot import types
from ah_bot import bot

import config
from ah_func import log_action, log_error, clear_vars, last_inline_set, last_inline_clear

#security_bot = telebot.TeleBot(config.SECURITY_TOKEN)

def queue_get_num(chat_id):
    bot.send_message(chat_id,'Введите 3 значный внутренний номер:')
    return

def queue_start(chat_id):
    action_stage = config.current_actions.get(chat_id)
    queue_members = ast_show_q()
    queues = fined_queues(action_stage[1],queue_members)
    if len(queues) == 0:
        bot.send_message(chat_id,'Добавочный номер '+action_stage[1]+' не состоит в очередях.')
        log_action('queue_start',chat_id,'Добавочный номер {!s} не состоит в очередях.'.format(action_stage[1]))
        send_log('#pbx #pbxqueue #pbxqueuefail','Добавочный номер ' +\
                 action_stage[1] + ' не состоит в очередях.',chat_id)
        clear_vars(chat_id)
        return
 
    bot.send_message(chat_id,'Добавочный номер состоит в очередях:\n'+str(queues))
    log_action('queue_start',chat_id,'Добавочный номер {!s} состоит в очередях: {!s}'.format(action_stage[1],queues))
    send_log('#pbx #pbxqueue #pbxqueuesuccess','Добавочный номер ' +\
             action_stage[1] + ' состоит в очередях: ' +\
             str(queues),chat_id)
    
    config.current_actions[chat_id] = (action_stage[0],{action_stage[1]:queues})
    kb = types.InlineKeyboardMarkup()
    kb.add(*[types.InlineKeyboardButton(text=text, callback_data="queue-accept-" + text) for text in ['да', 'нет']])
    inl = bot.send_message(chat_id, 'убрать из очередей?', reply_markup=kb)
    last_inline_set(chat_id,inl.message_id)
    return

def queue_remove_accept(call):
    bot.answer_callback_query(callback_query_id=call.id, show_alert=False, text="")
    chat_id=call.from_user.id
    if call.data[13:] == 'да':
        action_stage = config.current_actions.get(chat_id)
        member = [*action_stage[1].keys()][0]
        queue_remove = remove_member_all(member,action_stage[1][member])
        if len(queue_remove) == 0:
            bot.edit_message_text('Агент ' + member +
                              ' не состоит в динамических очередях.', call.from_user.id, call.message.message_id)
            log_action('queue_remove_accept',chat_id,'Агент {!s} не состоит в динамических очередях.'.format(member))
            clear_vars(call.from_user.id)
            return

        bot.edit_message_text('Готово. Агент ' + member +
                              ' удален из динамических очередей: '+ str(queue_remove), call.from_user.id, call.message.message_id)
        
        send_log('#pbx #pbxqueue #pbxqueueremove','Добавочный номeр ' +\
             member + ' удален из очередей: ' +\
             str(queue_remove),chat_id)

        #security_bot.send_message(config.SECURITY_CHANNEL,
        #                          '\U0000260E #pbx #pbxqueues\n'+str(datetime.datetime.strftime(datetime.datetime.now(), "%d %B %Y г. %H:%M:%S"))+'\n'+
        #                          'Закатать в цемент: '+config.ldap_mass[int(chat_id)]['sAMAccountName']+'\n'+
        #                          'Удалил из очередей '+str(queue_remove)+' агента '+member  
        #                         )
        log_action('queue_remove_accept',chat_id,'Готово. Агент  {!s} удален из динамических очередей:: {!s}'.format(member,queue_remove))
        clear_vars(call.from_user.id)
        return
    if call.data[13:] == 'нет':
        try:
            last_inline_clear(call.from_user.id)
        except:
            pass 
        clear_vars(call.from_user.id) 
        return

def split_t(line_t,mark,return_t):
    try:
        res = line_t.split(mark)[1]
        return line_t.split(mark)[return_t]
    except:
        return False

def ast_show_q():
    ast_queues = ami_send_action({'action':'queues'})
    members=[]
    queue_members={}
    if ast_queues == False:
        return False

    for line in ast_queues.split('\n'):
        queue = split_t(line,'strategy',0)
        if queue:
            try:
                queue_members[queue_now]=members
                members=[]
            except:
                pass
            queue_now=split_t(queue,' has',0)
            continue

        member = split_t(line,'SIP/',1)
        if member:
            members.append(split_t(member,' ',0))
    return queue_members

def ami_send_action(cdict={}):
    timeout = 2
    URL = config.AMI_HOST+':'+config.AMI_PORT+'/rawman'
    params = {'action':'login',
              'username':config.AMI_LOGIN,
              'secret':config.AMI_PASS,
              'events':'off',
             }
    #пробуем авторизоваться в asterisk AMI
    try:
        conn = requests.get(URL, params=params, timeout=timeout)
        if conn.text.split('Response: ')[1].split('\r')[0] == 'Error':
            raise Exception(conn.content)
    except Exception as ex:
        log_error('AMI connection Error: ',eX)
        return False

    #пробуем выполнить команду
    try:
        action = requests.get(URL, params=cdict, cookies=conn.cookies.get_dict(), timeout=timeout)
    except Exception as ex:
        log_error('AMI action Error: ',eX)
        return False

    params['action']='logoff'
    #разлогиниваемся
    try:
        disconn = requests.get(URL, params=params, timeout=timeout)
    except Exception as ex:
        log_error('AMI logout Error: ',eX)

    return action.text

def fined_queues(member,queue_members={}):
    queues=[]
    for key in queue_members:
        if member in queue_members[key]:
            queues.append(key)
    return queues

def remove_member_all(member,queues=[]):
    q=[]
    for queue in queues:
        status = ami_send_action({'action':'QueueRemove','Queue':queue,'Interface':'SIP/'+member})
        if status.split('Response: ')[1].split('\r')[0] != 'Error':
            q.append(queue)
    return q


def send_log(tags,func,chat_id):
    bot.send_message(config.HELPER_LOG_CHANNEL,\
                    '\U0000260E '+tags+'\n'+
                    '<b>'+func+'</b>'+'\n'+
                    'Пользователь: '+config.ldap_mass[int(chat_id)]['CanonicalName']+'\n'+
                    'AD: '+config.ldap_mass[int(chat_id)]['sAMAccountName']+'\n'+'\n'+
                    str(datetime.datetime.strftime(datetime.datetime.now(), "%d %B %Y г. %H:%M:%S")),
                    parse_mode="HTML",
                    )

