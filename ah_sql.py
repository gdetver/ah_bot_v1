import pymysql as mdb
import telebot
from ah_bot import bot

import config
from ah_func import log_error

#bot = telebot.TeleBot(config.TOKEN)


def mdb_connect(sql, query):
    try:
        status = 'ok'
        results = ''
        conn = mdb.connect(host=config.DB_HOST, user=config.DB_USER, passwd=config.DB_PASS,
                           db='pbx', charset='utf8')
        cursor = conn.cursor()
        try:
            cursor.execute(sql)
            if query == 'insert':
                conn.commit()
            results = cursor.fetchall()

        except Exception as eX:
            log_error('mdb_connect',eX)
            status = 'error'

    except Exception as eX:
        log_error('mdb_connect',eX)
        status = 'error'
    try:
        cursor.close()
        conn.close()
        return results, status
    except:
        return results, status


def select_src_mdb(chat_id, number, start_int, stop_int):
    sql = "select substr(src,-10,10) as source from pbx_cdr where calldate BETWEEN '" + start_int + "' AND '"\
             + stop_int + "' and src like '%" + number + "' and length(src)>9 group by source"
    results, status = mdb_connect(sql, 'select')
    if status != 'ok':
        bot.send_message(chat_id, 'ошибка БД обратитесь к системному администратору')
    return results

def select_rec_mdb(chat_id, number, start_int, stop_int):
    sql = "select record, calldate, src, dst, userinterface, username, agent_id, billsec, disposition\
             from pbx_cdr where calldate BETWEEN '" + start_int + "' AND '" + stop_int +\
             "' and (src like '%" + number + "' or dst like '%" + number + "' or userinterface like '%" + number + "')"

    results, status = mdb_connect(sql, 'select')
    if status != 'ok':
        bot.send_message(chat_id, 'ошибка БД обратитесь к системному администратору')
    return results

def select_blacklist_mdb(chat_id, number):
    sql = "select expiration_date from pbx_blacklist where callerid like '%" + number + "'"

    results, status = mdb_connect(sql, 'select')
    return results

def insert_blacklist_mdb(chat_id, number,exp_date, description):
    sql = "INSERT INTO pbx_blacklist (callerid, expiration_date, description) " \
          "VALUES ('" + number + "', '" + exp_date + "', '" + description + "')"
    results, status = mdb_connect(sql, 'insert')
    return

def update_blacklist_mdb(chat_id, number,exp_date):
    sql = "UPDATE pbx_blacklist SET expiration_date = '" + exp_date + "' WHERE callerid like '%" + number + "'"
    results, status = mdb_connect(sql, 'insert')
    return

def select_abandon_agent(chat_id,number,start_int):
    sql = "select time, callid FROM  pbx_queue_log force INDEX (index_time)\
            LEFT JOIN pbx_cdr on callid = uniqueid\
                WHERE time BETWEEN '"+str(start_int)+"' AND NOW()\
                AND event in ('ABANDON')\
                and src like '%"+ str(number) +"'"
    results, status = mdb_connect(sql, 'select')
    return results
