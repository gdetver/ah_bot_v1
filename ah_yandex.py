import paramiko

import telebot
from telebot import types
from ah_bot import bot
import datetime

import config
from ah_func import log_action, log_error, clear_vars, last_inline_set

#bot = telebot.TeleBot(config.TOKEN)

def ya_start(chat_id):
    markup = types.InlineKeyboardMarkup()
    row = []
    row.append(types.InlineKeyboardButton("проверить карты", callback_data="yandex-check"))
    markup.row(*row)
    row = []
    row.append(types.InlineKeyboardButton("сменить VPN", callback_data="yandex-replace"))
    markup.row(*row)
    row = []
    row.append(types.InlineKeyboardButton("Проверить и сменить", callback_data="yandex-auto"))
    markup.row(*row)
    inl = bot.send_message(chat_id, 'Что вы хотите сделать:', reply_markup=markup)
    last_inline_set(chat_id,inl.message_id)

def ya_action(chat_id):
    action_stage = config.current_actions.get(chat_id)
    log_action('ya_action',chat_id, 'выбранно действие:  {!s}.'\
                    .format(action_stage[1]))

    if action_stage[1] == "check":
        command = "/root/yandex_scr/checkyandex.py -c"
    elif action_stage[1] == "replace":
        command = "/root/yandex_scr/checkyandex.py -r"
    elif action_stage[1] == "auto":
        command = "/root/yandex_scr/checkyandex.py -a"
    else:
        log_error('ya_action','неизвестная ошибка кода')
        bot.send_message(chat_id,'неизвестная ошибка')
        clear_vars(chat_id)
        return

    bgp_console = ya_bgp_ssh(chat_id,command)

    if bgp_console == "":
        bot.send_message(chat_id,'ошибка подключения к bgp')
    else:
        bot.send_message(chat_id,bgp_console)
        if action_stage[1] == "replace":
            bot.send_message('-1001114524040',\
                            '\U0001F648 #yandex'+'\n'+
                            '<b>Смена VPN</b>'+'\n'+
                            'Вывод консоли: '+'\n'+
                            str(bgp_console)+'\n'+'\n'+
                            'Пользователь: '+config.ldap_mass[int(chat_id)]['CanonicalName']+'\n'+
                            'AD: '+config.ldap_mass[int(chat_id)]['sAMAccountName']+'\n'+
                            str(datetime.datetime.strftime(datetime.datetime.now(), "%d %B %Y г. %H:%M:%S")),
                            parse_mode="HTML",
                            )
    clear_vars(chat_id)
    return

def ya_bgp_ssh(chat_id,command):
    try:
        sshcon   = paramiko.SSHClient()  # will create the object
        sshcon.set_missing_host_key_policy(paramiko.AutoAddPolicy())# no known_hosts error
        sshcon.connect(config.SSH_BGP_HOST, username=config.SSH_BGP_USER, key_filename=config.SSH_BGP_KEY)
        stdin, stdout, stderr = sshcon.exec_command(command)
        data = stdout.read() + stderr.read()
        sshcon.close()
        log_action('ya_bgp_ssh',chat_id, 'вывод консоли:  {!s}.'\
                    .format(data))
        return data
    except Exception as eX:
        log_error('ssh_client',eX)
        clear_vars(chat_id)
        return ""




