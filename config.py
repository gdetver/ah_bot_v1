# -*- coding: utf-8 -*-
LDAP_HOST = "ad.gazelkin.local"
LDAP_USER = "astbot_search@gazelkin.local"
LDAP_PASS = ""

TOKEN = ""
#TOKEN = ""
SECURITY_TOKEN = ""
SECURITY_CHANNEL = "-1001261416282"
HELPER_LOG_CHANNEL = '-1001378974490'

DB_HOST = "pbx-db.gazelkin.local"
DB_USER = "ah_bot"
DB_PASS = ""

SSH_DMP_HOST = 'hq-dmp-01.gazelkin.local'
SSH_DMP_USER = 'ahbot'
SSH_DMP_SECRET = ''
SSH_DMP_PORT = 22

SSH_BGP_HOST = 'bgp.gazelkin.local'
SSH_BGP_USER = 'root'
SSH_BGP_KEY = './ssh/ssh.private'

SSH_AST_HOST = 'pbx.gazelkin.local'
SSH_AST_USER = 'log'
SSH_AST_KEY = './ssh/ssh.private'

AMI_HOST='http://pbx.gazelkin.local'
AMI_PORT='8088'
AMI_LOGIN='testic'
AMI_PASS='234534645657'

REC_DIR = '/home/ahbot/tmp/records/'

WEBHOOK_HOST = 'astbot.gazelkin.ru/prod'
WEBHOOK_PORT = 10001
WEBHOOK_LISTEN = '127.0.0.1'

WEBHOOK_URL_BASE = "https://%s" % (WEBHOOK_HOST)
WEBHOOK_URL_PATH = "/%s/" % (TOKEN)

# Интервал раскрытия номера при добавлении в бан(в часах)
ban_decrypt_hour = 3

#группы АД
ldap_all = ['Astbot_admins',
            'Astbot_blacklist_add',
            'Astbot_number_disclosure',
            'Astbot_records_download',
            'Astbot_abandon_agent',
            'Astbot_yandex_map',
            'Astbot_queue_remove_all'
            ]

ldap_blacklist_add = ['Astbot_admins','Astbot_blacklist_add']
#ldap_blacklist_del = ['Astbot_admins','Astbot_blacklist_del']
ldap_number_disclosure = ['Astbot_admins','Astbot_number_disclosure']
ldap_records_download = ['Astbot_admins','Astbot_records_download']
ldap_abandon_agent = ['Astbot_admins','Astbot_abandon_agent']
ldap_yandex_map_check = ['Astbot_admins','Astbot_yandex_map']
ldap_queue_remove_all = ['Astbot_admins','Astbot_queue_remove_all']
ldap_admins = ['Astbot_admins']

#кнопки
ldap_buttons = {'Astbot_admins':['раскрыть номер', 'добавить в бан','выгрузить записи','кто пропустил','проверить очереди','yandex map'],
                'Astbot_blacklist_add':['добавить в бан'],
                'Astbot_number_disclosure':['раскрыть номер'],
                'Astbot_records_download':['выгрузить записи'],
                'Astbot_abandon_agent':['кто пропустил'],
                'Astbot_queue_remove_all':['проверить очереди'],
                'Astbot_yandex_map':['yandex map']
                }


#переменные окружения пользователя
current_shown_datetime = {}
current_actions = {}
current_justification = {}
ldap_mass = {}
last_inline = {}






